//
//  _0230315_PiyushGoel_NYCSchoolsTests.swift
//  20230315-PiyushGoel-NYCSchoolsTests
//
//  Created by PG on 3/15/23.
//

import XCTest
@testable import _0230315_PiyushGoel_NYCSchools

final class _0230315_PiyushGoel_NYCSchoolsTests: XCTestCase {
    
    //MARK:- UnitTest Case for SchoolParser API for Valid Response
    func test_SchoolParserAPI_WithValidURL_Returns_SchoolAPIResponse() async throws
    {
        //ACT
        let schoolDataResponse = try await HTTPUtility.shared.performSchoolOperation(url: Service.schoolURL, queryItem: "", response: [SchoolDetail].self)
        
        let firstDataValue = schoolDataResponse.first!
        //ASSERT
        XCTAssertNotNil(schoolDataResponse)
        XCTAssertEqual("Clinton School Writers & Artists, M.S. 260",firstDataValue.schoolName)
        XCTAssertEqual("02M260", firstDataValue.dbNumber)
        
    }
    
    //MARK:- UnitTest Case for SATScoreParser API for Valid Response
    func test_SATScoreParserAPI_WithValidURL_Returns_SATScoreResponse() async throws
    {
        //ACT
        let SATScoreResponse = try await HTTPUtility.shared.performSchoolOperation(url: Service.SATScoreURL, queryItem:"01M292", response: [SATScoreDetail].self)
        
        let firstDataValue = SATScoreResponse.first!
        //ASSERT
        XCTAssertNotNil(SATScoreResponse)
        XCTAssertEqual("404",firstDataValue.mathScore)
        XCTAssertEqual("355", firstDataValue.readingScore)
        XCTAssertEqual("363", firstDataValue.writingScore)
    }
}
