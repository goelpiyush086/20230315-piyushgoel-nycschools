//
//  HTTPUtility.swift
//  20230315-PiyushGoel-NYCSchools
//
//  Created by PG on 3/15/23.
//

import Foundation

//MARK:- Enum created for httpError
enum httpError: Error {
    case BadURL
    case nonSuccessStatusCode
    case jsonDecoding
}

//MARK:- HTTPUtilityDelegate protocol
protocol HTTPUtilityDelegate {
    func performSchoolOperation<T:Decodable>(url: String, queryItem: String, response: [T].Type) async throws -> [T]
}

//MARK:- HTTPUtility class
final class HTTPUtility: HTTPUtilityDelegate {
    
    static let shared = HTTPUtility()
    private init() {}
    
    func performSchoolOperation<T: Decodable>(url: String, queryItem: String, response: [T].Type) async throws -> [T] {
        
        do {
            guard let url = URL(string: url + queryItem) else {
                throw httpError.BadURL
            }
            
            let (serverData, serverURLResponse) = try await URLSession.shared.data(from: url)
            
            guard let httpStatusCode = (serverURLResponse as? HTTPURLResponse)?.statusCode, (200...299).contains(httpStatusCode) else {
                throw httpError.nonSuccessStatusCode
            }
            return try JSONDecoder().decode(response, from: serverData)
        } catch {
            throw httpError.jsonDecoding
        }
    }
}
