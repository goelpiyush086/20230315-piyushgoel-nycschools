//
//  _0230315_PiyushGoel_NYCSchoolsApp.swift
//  20230315-PiyushGoel-NYCSchools
//
//  Created by PG on 3/15/23.
//

import SwiftUI

@main
struct _0230315_PiyushGoel_NYCSchoolsApp: App {
    var body: some Scene {
        WindowGroup {
            SchoolView()
        }
    }
}
