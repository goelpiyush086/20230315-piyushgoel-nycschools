//
//  DetailView.swift
//  20230315-PiyushGoel-NYCSchools
//
//  Created by PG on 3/15/23.
//

import SwiftUI

//MARK:- Declare DetailView

@MainActor
struct DetailView: View {
    
    @ObservedObject var viewModel: SchoolViewModel
    @Environment(\.presentationMode) var presentationMode
    var dbnValue: String
    
    var body: some View {
        NavigationView {
            ZStack {
                Color.yellow.edgesIgnoringSafeArea(.all)
                VStack  {
                    ForEach(viewModel.satScoreDetail, id: \.self) {
                        item in
                        Text("\(Constants.ReadingScore) \(item.readingScore)")
                            .padding(20)
                            .foregroundColor(.blue)
                        Text("\(Constants.MathScore) \(item.mathScore)")
                            .padding(20)
                            .foregroundColor(.green)
                        Text("\(Constants.WritingScore) \(item.writingScore)")
                            .padding(20)
                            .padding(.bottom, 350)
                            .foregroundColor(.red)
                    }
                }
                .edgesIgnoringSafeArea(.all)
                .navigationBarTitleDisplayMode(.inline)
                .toolbar {
                    ToolbarItem(placement: .principal) {
                        Text(Constants.detailViewTitle)
                            .font(.largeTitle.bold())
                            .accessibilityAddTraits(.isHeader)
                    }
                }
                .font(.largeTitle)
                .onAppear {
                    Task {
                        await viewModel.getSATScoreDetail(queryValue: dbnValue)
                    }
                }
                .alert(isPresented: $viewModel.showAlert) {
                    Alert(title: Text(Constants.alertTitle),
                          message: Text(Constants.alertMessage),
                          dismissButton: Alert.Button.default(
                            Text(Constants.okMessage), action: {
                                presentationMode.wrappedValue.dismiss()
                            }
                          )
                    )
                }
            }
        }
    }
}

struct DetailView_Previews: PreviewProvider {
    static var previews: some View {
        DetailView(viewModel: SchoolViewModel(), dbnValue: "")
    }
}
